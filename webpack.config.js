const path = require("path");

PASTH = 
{
  mainPath: "C:\\Users\\BelovAA\\Desktop\\vue-project\\"
}
module.exports = {
  externals: {
    paths: PATHS
  },
  mode: `development`,
  context: `C:\\Users\\BelovAA\\Desktop\\vue-project\\`,
  node: {
    setImmediate: false,
    process: `mock`,
    dgram: `empty`,
    fs: `empty`,
    net: `empty`,
    tls: `empty`,
    child_process: `empty`
  },
  output: {
    path: `${PASTH.mainPath}dist`,
    filename: `js/[name].js`,
    publicPath: `/`,
    chunkFilename: `js/[name].js`
  },
  resolve: {
    alias: {
      '@': `${PASTH.mainPath}src`,
      vue$: `vue/dist/vue.runtime.esm.js`
    },
    extensions: [
      `.mjs`,
      `.js`,
      `.jsx`,
      `.vue`,
      `.json`,
      `.wasm`
    ],
    modules: [
      `node_modules`,
      `${PASTH.mainPath}node_modules`,
      `${PASTH.mainPath}node_modules\\@vue\\cli-service\\node_modules`
    ]
  },
  resolveLoader: {
    modules: [
      `${PASTH.mainPath}node_modules\\@vue\\cli-plugin-babel\\node_modules`,
      `node_modules`,
      `${PASTH.mainPath}node_modules`,
      `${PASTH.mainPath}node_modules\\@vue\\cli-service\\node_modules`
    ]
  },
  module: {
    noParse: /^(vue|vue-router|vuex|vuex-router-sync)$/,
    rules: [
      {
        test: /\.vue$/,
        use: [
          {
            loader: `${PASTH.mainPath}node_modules\\cache-loader\\dist\\cjs.js`,
            options: {
              cacheDirectory: `${PASTH.mainPath}node_modules\\.cache\\vue-loader`,
              cacheIdentifier: `3011368a`
            }
          },
          {
            loader: `${PASTH.mainPath}node_modules\\vue-loader\\lib\\main.js`,
            options: {
              compilerOptions: {
                whitespace: `condense`
              },
              cacheDirectory: `${PASTH.mainPath}node_modules\\.cache\\vue-loader`,
              cacheIdentifier: `3011368a`
            }
          }
        ]
      },
      {
        test: /\.(png|jpe?g|gif|webp)(\?.*)?$/,
        use: [
          {
            loader: `${PASTH.mainPath}node_modules\\url-loader\\dist\\cjs.js`,
            options: {
              limit: 4096,
              fallback: {
                loader: `file-loader`,
                options: {
                  name: `img/[name].[hash:8].[ext]`
                }
              }
            }
          }
        ]
      },
      {
        test: /\.(svg)(\?.*)?$/,
        use: [
          {
            loader: `${PASTH.mainPath}node_modules\\file-loader\\dist\\cjs.js`,
            options: {
              name: `img/[name].[hash:8].[ext]`
            }
          }
        ]
      },
      {
        test: /\.(mp4|webm|ogg|mp3|wav|flac|aac)(\?.*)?$/,
        use: [
          {
            loader: `${PASTH.mainPath}node_modules\\url-loader\\dist\\cjs.js`,
            options: {
              limit: 4096,
              fallback: {
                loader: `file-loader`,
                options: {
                  name: `media/[name].[hash:8].[ext]`
                }
              }
            }
          }
        ]
      },
      {
        test: /\.(woff2?|eot|ttf|otf)(\?.*)?$/i,
        use: [
          {
            loader: `${PASTH.mainPath}node_modules\\url-loader\\dist\\cjs.js`,
            options: {
              limit: 4096,
              fallback: {
                loader: `file-loader`,
                options: {
                  name: `fonts/[name].[hash:8].[ext]`
                }
              }
            }
          }
        ]
      },
      {
        test: /\.pug$/,
        oneOf: [
          {
            resourceQuery: /vue/,
            use: [
              {
                loader: `pug-plain-loader`
              }
            ]
          },
          {
            use: [
              {
                loader: `raw-loader`
              },
              {
                loader: `pug-plain-loader`
              }
            ]
          }
        ]
      },
      {
        test: /\.css$/,
        oneOf: [
          {
            resourceQuery: /module/,
            use: [
              {
                loader: `${PASTH.mainPath}node_modules\\vue-style-loader\\main.js`,
                options: {
                  sourceMap: false,
                  shadowMode: false
                }
              },
              {
                loader: `${PASTH.mainPath}node_modules\\css-loader\\dist\\cjs.js`,
                options: {
                  sourceMap: false,
                  importLoaders: 2,
                  modules: {
                    localIdentName: `[name]_[local]_[hash:base64:5]`
                  }
                }
              },
              {
                loader: `${PASTH.mainPath}node_modules\\postcss-loader\\src\\main.js`,
                options: {
                  sourceMap: false,
                  plugins: [
                    function () { /* omitted long function */ }
                  ]
                }
              }
            ]
          },
          {
            resourceQuery: /\?vue/,
            use: [
              {
                loader: `${PASTH.mainPath}node_modules\\vue-style-loader\\main.js`,
                options: {
                  sourceMap: false,
                  shadowMode: false
                }
              },
              {
                loader: `${PASTH.mainPath}node_modules\\css-loader\\dist\\cjs.js`,
                options: {
                  sourceMap: false,
                  importLoaders: 2
                }
              },
              {
                loader: `${PASTH.mainPath}node_modules\\postcss-loader\\src\\main.js`,
                options: {
                  sourceMap: false,
                  plugins: [
                    function () {  }
                  ]
                }
              }
            ]
          },
          {
            test: /\.module\.\w+$/,
            use: [
              {
                loader: `${PASTH.mainPath}node_modules\\vue-style-loader\\main.js`,
                options: {
                  sourceMap: false,
                  shadowMode: false
                }
              },
              {
                loader: `${PASTH.mainPath}node_modules\\css-loader\\dist\\cjs.js`,
                options: {
                  sourceMap: false,
                  importLoaders: 2,
                  modules: {
                    localIdentName: `[name]_[local]_[hash:base64:5]`
                  }
                }
              },
              {
                loader: `${PASTH.mainPath}node_modules\\postcss-loader\\src\\main.js`,
                options: {
                  sourceMap: false,
                  plugins: [
                    function () { /* omitted long function */ }
                  ]
                }
              }
            ]
          },
          {
            use: [
              {
                loader: `${PASTH.mainPath}node_modules\\vue-style-loader\\main.js`,
                options: {
                  sourceMap: false,
                  shadowMode: false
                }
              },
              {
                loader: `${PASTH.mainPath}node_modules\\css-loader\\dist\\cjs.js`,
                options: {
                  sourceMap: false,
                  importLoaders: 2
                }
              },
              {
                loader: `${PASTH.mainPath}node_modules\\postcss-loader\\src\\main.js`,
                options: {
                  sourceMap: false,
                  plugins: [
                    function () { /* omitted long function */ }
                  ]
                }
              }
            ]
          }
        ]
      },
      {
        test: /\.p(ost)?css$/,
        oneOf: [
          /* config.module.rule(`postcss`).oneOf(`vue-modules`) */
          {
            resourceQuery: /module/,
            use: [
              /* config.module.rule(`postcss`).oneOf(`vue-modules`).use(`vue-style-loader`) */
              {
                loader: `${PASTH.mainPath}node_modules\\vue-style-loader\\main.js`,
                options: {
                  sourceMap: false,
                  shadowMode: false
                }
              },
              /* config.module.rule(`postcss`).oneOf(`vue-modules`).use(`css-loader`) */
              {
                loader: `${PASTH.mainPath}node_modules\\css-loader\\dist\\cjs.js`,
                options: {
                  sourceMap: false,
                  importLoaders: 2,
                  modules: {
                    localIdentName: `[name]_[local]_[hash:base64:5]`
                  }
                }
              },
              /* config.module.rule(`postcss`).oneOf(`vue-modules`).use(`postcss-loader`) */
              {
                loader: `${PASTH.mainPath}node_modules\\postcss-loader\\src\\main.js`,
                options: {
                  sourceMap: false,
                  plugins: [
                    function () { /* omitted long function */ }
                  ]
                }
              }
            ]
          },
          /* config.module.rule(`postcss`).oneOf(`vue`) */
          {
            resourceQuery: /\?vue/,
            use: [
              /* config.module.rule(`postcss`).oneOf(`vue`).use(`vue-style-loader`) */
              {
                loader: `${PASTH.mainPath}node_modules\\vue-style-loader\\main.js`,
                options: {
                  sourceMap: false,
                  shadowMode: false
                }
              },
              /* config.module.rule(`postcss`).oneOf(`vue`).use(`css-loader`) */
              {
                loader: `${PASTH.mainPath}node_modules\\css-loader\\dist\\cjs.js`,
                options: {
                  sourceMap: false,
                  importLoaders: 2
                }
              },
              /* config.module.rule(`postcss`).oneOf(`vue`).use(`postcss-loader`) */
              {
                loader: `${PASTH.mainPath}node_modules\\postcss-loader\\src\\main.js`,
                options: {
                  sourceMap: false,
                  plugins: [
                    function () { /* omitted long function */ }
                  ]
                }
              }
            ]
          },
          /* config.module.rule(`postcss`).oneOf(`normal-modules`) */
          {
            test: /\.module\.\w+$/,
            use: [
              /* config.module.rule(`postcss`).oneOf(`normal-modules`).use(`vue-style-loader`) */
              {
                loader: `${PASTH.mainPath}node_modules\\vue-style-loader\\main.js`,
                options: {
                  sourceMap: false,
                  shadowMode: false
                }
              },
              /* config.module.rule(`postcss`).oneOf(`normal-modules`).use(`css-loader`) */
              {
                loader: `${PASTH.mainPath}node_modules\\css-loader\\dist\\cjs.js`,
                options: {
                  sourceMap: false,
                  importLoaders: 2,
                  modules: {
                    localIdentName: `[name]_[local]_[hash:base64:5]`
                  }
                }
              },
              /* config.module.rule(`postcss`).oneOf(`normal-modules`).use(`postcss-loader`) */
              {
                loader: `${PASTH.mainPath}node_modules\\postcss-loader\\src\\main.js`,
                options: {
                  sourceMap: false,
                  plugins: [
                    function () { /* omitted long function */ }
                  ]
                }
              }
            ]
          },
          /* config.module.rule(`postcss`).oneOf(`normal`) */
          {
            use: [
              /* config.module.rule(`postcss`).oneOf(`normal`).use(`vue-style-loader`) */
              {
                loader: `${PASTH.mainPath}node_modules\\vue-style-loader\\main.js`,
                options: {
                  sourceMap: false,
                  shadowMode: false
                }
              },
              /* config.module.rule(`postcss`).oneOf(`normal`).use(`css-loader`) */
              {
                loader: `${PASTH.mainPath}node_modules\\css-loader\\dist\\cjs.js`,
                options: {
                  sourceMap: false,
                  importLoaders: 2
                }
              },
              /* config.module.rule(`postcss`).oneOf(`normal`).use(`postcss-loader`) */
              {
                loader: `${PASTH.mainPath}node_modules\\postcss-loader\\src\\main.js`,
                options: {
                  sourceMap: false,
                  plugins: [
                    function () { /* omitted long function */ }
                  ]
                }
              }
            ]
          }
        ]
      },
      /* config.module.rule(`scss`) */
      {
        test: /\.scss$/,
        oneOf: [
          /* config.module.rule(`scss`).oneOf(`vue-modules`) */
          {
            resourceQuery: /module/,
            use: [
              /* config.module.rule(`scss`).oneOf(`vue-modules`).use(`vue-style-loader`) */
              {
                loader: `${PASTH.mainPath}node_modules\\vue-style-loader\\main.js`,
                options: {
                  sourceMap: false,
                  shadowMode: false
                }
              },
              /* config.module.rule(`scss`).oneOf(`vue-modules`).use(`css-loader`) */
              {
                loader: `${PASTH.mainPath}node_modules\\css-loader\\dist\\cjs.js`,
                options: {
                  sourceMap: false,
                  importLoaders: 2,
                  modules: {
                    localIdentName: `[name]_[local]_[hash:base64:5]`
                  }
                }
              },
              /* config.module.rule(`scss`).oneOf(`vue-modules`).use(`postcss-loader`) */
              {
                loader: `${PASTH.mainPath}node_modules\\postcss-loader\\src\\main.js`,
                options: {
                  sourceMap: false,
                  plugins: [
                    function () { /* omitted long function */ }
                  ]
                }
              },
              /* config.module.rule(`scss`).oneOf(`vue-modules`).use(`sass-loader`) */
              {
                loader: `sass-loader`,
                options: {
                  sourceMap: false
                }
              }
            ]
          },
          /* config.module.rule(`scss`).oneOf(`vue`) */
          {
            resourceQuery: /\?vue/,
            use: [
              /* config.module.rule(`scss`).oneOf(`vue`).use(`vue-style-loader`) */
              {
                loader: `${PASTH.mainPath}node_modules\\vue-style-loader\\main.js`,
                options: {
                  sourceMap: false,
                  shadowMode: false
                }
              },
              /* config.module.rule(`scss`).oneOf(`vue`).use(`css-loader`) */
              {
                loader: `${PASTH.mainPath}node_modules\\css-loader\\dist\\cjs.js`,
                options: {
                  sourceMap: false,
                  importLoaders: 2
                }
              },
              /* config.module.rule(`scss`).oneOf(`vue`).use(`postcss-loader`) */
              {
                loader: `${PASTH.mainPath}node_modules\\postcss-loader\\src\\main.js`,
                options: {
                  sourceMap: false,
                  plugins: [
                    function () { /* omitted long function */ }
                  ]
                }
              },
              /* config.module.rule(`scss`).oneOf(`vue`).use(`sass-loader`) */
              {
                loader: `sass-loader`,
                options: {
                  sourceMap: false
                }
              }
            ]
          },
          /* config.module.rule(`scss`).oneOf(`normal-modules`) */
          {
            test: /\.module\.\w+$/,
            use: [
              /* config.module.rule(`scss`).oneOf(`normal-modules`).use(`vue-style-loader`) */
              {
                loader: `${PASTH.mainPath}node_modules\\vue-style-loader\\main.js`,
                options: {
                  sourceMap: false,
                  shadowMode: false
                }
              },
              /* config.module.rule(`scss`).oneOf(`normal-modules`).use(`css-loader`) */
              {
                loader: `${PASTH.mainPath}node_modules\\css-loader\\dist\\cjs.js`,
                options: {
                  sourceMap: false,
                  importLoaders: 2,
                  modules: {
                    localIdentName: `[name]_[local]_[hash:base64:5]`
                  }
                }
              },
              /* config.module.rule(`scss`).oneOf(`normal-modules`).use(`postcss-loader`) */
              {
                loader: `${PASTH.mainPath}node_modules\\postcss-loader\\src\\main.js`,
                options: {
                  sourceMap: false,
                  plugins: [
                    function () { /* omitted long function */ }
                  ]
                }
              },
              /* config.module.rule(`scss`).oneOf(`normal-modules`).use(`sass-loader`) */
              {
                loader: `sass-loader`,
                options: {
                  sourceMap: false
                }
              }
            ]
          },
          /* config.module.rule(`scss`).oneOf(`normal`) */
          {
            use: [
              /* config.module.rule(`scss`).oneOf(`normal`).use(`vue-style-loader`) */
              {
                loader: `${PASTH.mainPath}node_modules\\vue-style-loader\\main.js`,
                options: {
                  sourceMap: false,
                  shadowMode: false
                }
              },
              /* config.module.rule(`scss`).oneOf(`normal`).use(`css-loader`) */
              {
                loader: `${PASTH.mainPath}node_modules\\css-loader\\dist\\cjs.js`,
                options: {
                  sourceMap: false,
                  importLoaders: 2
                }
              },
              /* config.module.rule(`scss`).oneOf(`normal`).use(`postcss-loader`) */
              {
                loader: `${PASTH.mainPath}node_modules\\postcss-loader\\src\\main.js`,
                options: {
                  sourceMap: false,
                  plugins: [
                    function () { /* omitted long function */ }
                  ]
                }
              },
              /* config.module.rule(`scss`).oneOf(`normal`).use(`sass-loader`) */
              {
                loader: `sass-loader`,
                options: {
                  sourceMap: false
                }
              }
            ]
          }
        ]
      },
      /* config.module.rule(`sass`) */
      {
        test: /\.sass$/,
        oneOf: [
          /* config.module.rule(`sass`).oneOf(`vue-modules`) */
          {
            resourceQuery: /module/,
            use: [
              /* config.module.rule(`sass`).oneOf(`vue-modules`).use(`vue-style-loader`) */
              {
                loader: `${PASTH.mainPath}node_modules\\vue-style-loader\\main.js`,
                options: {
                  sourceMap: false,
                  shadowMode: false
                }
              },
              /* config.module.rule(`sass`).oneOf(`vue-modules`).use(`css-loader`) */
              {
                loader: `${PASTH.mainPath}node_modules\\css-loader\\dist\\cjs.js`,
                options: {
                  sourceMap: false,
                  importLoaders: 2,
                  modules: {
                    localIdentName: `[name]_[local]_[hash:base64:5]`
                  }
                }
              },
              /* config.module.rule(`sass`).oneOf(`vue-modules`).use(`postcss-loader`) */
              {
                loader: `${PASTH.mainPath}node_modules\\postcss-loader\\src\\main.js`,
                options: {
                  sourceMap: false,
                  plugins: [
                    function () { /* omitted long function */ }
                  ]
                }
              },
              /* config.module.rule(`sass`).oneOf(`vue-modules`).use(`sass-loader`) */
              {
                loader: `sass-loader`,
                options: {
                  sourceMap: false,
                  sassOptions: {
                    indentedSyntax: true
                  }
                }
              }
            ]
          },
          /* config.module.rule(`sass`).oneOf(`vue`) */
          {
            resourceQuery: /\?vue/,
            use: [
              /* config.module.rule(`sass`).oneOf(`vue`).use(`vue-style-loader`) */
              {
                loader: `${PASTH.mainPath}node_modules\\vue-style-loader\\main.js`,
                options: {
                  sourceMap: false,
                  shadowMode: false
                }
              },
              /* config.module.rule(`sass`).oneOf(`vue`).use(`css-loader`) */
              {
                loader: `${PASTH.mainPath}node_modules\\css-loader\\dist\\cjs.js`,
                options: {
                  sourceMap: false,
                  importLoaders: 2
                }
              },
              /* config.module.rule(`sass`).oneOf(`vue`).use(`postcss-loader`) */
              {
                loader: `${PASTH.mainPath}node_modules\\postcss-loader\\src\\main.js`,
                options: {
                  sourceMap: false,
                  plugins: [
                    function () { /* omitted long function */ }
                  ]
                }
              },
              /* config.module.rule(`sass`).oneOf(`vue`).use(`sass-loader`) */
              {
                loader: `sass-loader`,
                options: {
                  sourceMap: false,
                  sassOptions: {
                    indentedSyntax: true
                  }
                }
              }
            ]
          },
          /* config.module.rule(`sass`).oneOf(`normal-modules`) */
          {
            test: /\.module\.\w+$/,
            use: [
              /* config.module.rule(`sass`).oneOf(`normal-modules`).use(`vue-style-loader`) */
              {
                loader: `${PASTH.mainPath}node_modules\\vue-style-loader\\main.js`,
                options: {
                  sourceMap: false,
                  shadowMode: false
                }
              },
              /* config.module.rule(`sass`).oneOf(`normal-modules`).use(`css-loader`) */
              {
                loader: `${PASTH.mainPath}node_modules\\css-loader\\dist\\cjs.js`,
                options: {
                  sourceMap: false,
                  importLoaders: 2,
                  modules: {
                    localIdentName: `[name]_[local]_[hash:base64:5]`
                  }
                }
              },
              /* config.module.rule(`sass`).oneOf(`normal-modules`).use(`postcss-loader`) */
              {
                loader: `${PASTH.mainPath}node_modules\\postcss-loader\\src\\main.js`,
                options: {
                  sourceMap: false,
                  plugins: [
                    function () { /* omitted long function */ }
                  ]
                }
              },
              /* config.module.rule(`sass`).oneOf(`normal-modules`).use(`sass-loader`) */
              {
                loader: `sass-loader`,
                options: {
                  sourceMap: false,
                  sassOptions: {
                    indentedSyntax: true
                  }
                }
              }
            ]
          },
          /* config.module.rule(`sass`).oneOf(`normal`) */
          {
            use: [
              /* config.module.rule(`sass`).oneOf(`normal`).use(`vue-style-loader`) */
              {
                loader: `${PASTH.mainPath}node_modules\\vue-style-loader\\main.js`,
                options: {
                  sourceMap: false,
                  shadowMode: false
                }
              },
              /* config.module.rule(`sass`).oneOf(`normal`).use(`css-loader`) */
              {
                loader: `${PASTH.mainPath}node_modules\\css-loader\\dist\\cjs.js`,
                options: {
                  sourceMap: false,
                  importLoaders: 2
                }
              },
              /* config.module.rule(`sass`).oneOf(`normal`).use(`postcss-loader`) */
              {
                loader: `${PASTH.mainPath}node_modules\\postcss-loader\\src\\main.js`,
                options: {
                  sourceMap: false,
                  plugins: [
                    function () { /* omitted long function */ }
                  ]
                }
              },
              /* config.module.rule(`sass`).oneOf(`normal`).use(`sass-loader`) */
              {
                loader: `sass-loader`,
                options: {
                  sourceMap: false,
                  sassOptions: {
                    indentedSyntax: true
                  }
                }
              }
            ]
          }
        ]
      },
      /* config.module.rule(`less`) */
      {
        test: /\.less$/,
        oneOf: [
          /* config.module.rule(`less`).oneOf(`vue-modules`) */
          {
            resourceQuery: /module/,
            use: [
              /* config.module.rule(`less`).oneOf(`vue-modules`).use(`vue-style-loader`) */
              {
                loader: `${PASTH.mainPath}node_modules\\vue-style-loader\\main.js`,
                options: {
                  sourceMap: false,
                  shadowMode: false
                }
              },
              /* config.module.rule(`less`).oneOf(`vue-modules`).use(`css-loader`) */
              {
                loader: `${PASTH.mainPath}node_modules\\css-loader\\dist\\cjs.js`,
                options: {
                  sourceMap: false,
                  importLoaders: 2,
                  modules: {
                    localIdentName: `[name]_[local]_[hash:base64:5]`
                  }
                }
              },
              /* config.module.rule(`less`).oneOf(`vue-modules`).use(`postcss-loader`) */
              {
                loader: `${PASTH.mainPath}node_modules\\postcss-loader\\src\\main.js`,
                options: {
                  sourceMap: false,
                  plugins: [
                    function () { /* omitted long function */ }
                  ]
                }
              },
              /* config.module.rule(`less`).oneOf(`vue-modules`).use(`less-loader`) */
              {
                loader: `less-loader`,
                options: {
                  sourceMap: false
                }
              }
            ]
          },
          /* config.module.rule(`less`).oneOf(`vue`) */
          {
            resourceQuery: /\?vue/,
            use: [
              /* config.module.rule(`less`).oneOf(`vue`).use(`vue-style-loader`) */
              {
                loader: `${PASTH.mainPath}node_modules\\vue-style-loader\\main.js`,
                options: {
                  sourceMap: false,
                  shadowMode: false
                }
              },
              /* config.module.rule(`less`).oneOf(`vue`).use(`css-loader`) */
              {
                loader: `${PASTH.mainPath}node_modules\\css-loader\\dist\\cjs.js`,
                options: {
                  sourceMap: false,
                  importLoaders: 2
                }
              },
              /* config.module.rule(`less`).oneOf(`vue`).use(`postcss-loader`) */
              {
                loader: `${PASTH.mainPath}node_modules\\postcss-loader\\src\\main.js`,
                options: {
                  sourceMap: false,
                  plugins: [
                    function () { /* omitted long function */ }
                  ]
                }
              },
              /* config.module.rule(`less`).oneOf(`vue`).use(`less-loader`) */
              {
                loader: `less-loader`,
                options: {
                  sourceMap: false
                }
              }
            ]
          },
          /* config.module.rule(`less`).oneOf(`normal-modules`) */
          {
            test: /\.module\.\w+$/,
            use: [
              /* config.module.rule(`less`).oneOf(`normal-modules`).use(`vue-style-loader`) */
              {
                loader: `${PASTH.mainPath}node_modules\\vue-style-loader\\main.js`,
                options: {
                  sourceMap: false,
                  shadowMode: false
                }
              },
              /* config.module.rule(`less`).oneOf(`normal-modules`).use(`css-loader`) */
              {
                loader: `${PASTH.mainPath}node_modules\\css-loader\\dist\\cjs.js`,
                options: {
                  sourceMap: false,
                  importLoaders: 2,
                  modules: {
                    localIdentName: `[name]_[local]_[hash:base64:5]`
                  }
                }
              },
              /* config.module.rule(`less`).oneOf(`normal-modules`).use(`postcss-loader`) */
              {
                loader: `${PASTH.mainPath}node_modules\\postcss-loader\\src\\main.js`,
                options: {
                  sourceMap: false,
                  plugins: [
                    function () { /* omitted long function */ }
                  ]
                }
              },
              /* config.module.rule(`less`).oneOf(`normal-modules`).use(`less-loader`) */
              {
                loader: `less-loader`,
                options: {
                  sourceMap: false
                }
              }
            ]
          },
          /* config.module.rule(`less`).oneOf(`normal`) */
          {
            use: [
              /* config.module.rule(`less`).oneOf(`normal`).use(`vue-style-loader`) */
              {
                loader: `${PASTH.mainPath}node_modules\\vue-style-loader\\main.js`,
                options: {
                  sourceMap: false,
                  shadowMode: false
                }
              },
              /* config.module.rule(`less`).oneOf(`normal`).use(`css-loader`) */
              {
                loader: `${PASTH.mainPath}node_modules\\css-loader\\dist\\cjs.js`,
                options: {
                  sourceMap: false,
                  importLoaders: 2
                }
              },
              /* config.module.rule(`less`).oneOf(`normal`).use(`postcss-loader`) */
              {
                loader: `${PASTH.mainPath}node_modules\\postcss-loader\\src\\main.js`,
                options: {
                  sourceMap: false,
                  plugins: [
                    function () { /* omitted long function */ }
                  ]
                }
              },
              /* config.module.rule(`less`).oneOf(`normal`).use(`less-loader`) */
              {
                loader: `less-loader`,
                options: {
                  sourceMap: false
                }
              }
            ]
          }
        ]
      },
      /* config.module.rule(`stylus`) */
      {
        test: /\.styl(us)?$/,
        oneOf: [
          /* config.module.rule(`stylus`).oneOf(`vue-modules`) */
          {
            resourceQuery: /module/,
            use: [
              /* config.module.rule(`stylus`).oneOf(`vue-modules`).use(`vue-style-loader`) */
              {
                loader: `${PASTH.mainPath}node_modules\\vue-style-loader\\main.js`,
                options: {
                  sourceMap: false,
                  shadowMode: false
                }
              },
              /* config.module.rule(`stylus`).oneOf(`vue-modules`).use(`css-loader`) */
              {
                loader: `${PASTH.mainPath}node_modules\\css-loader\\dist\\cjs.js`,
                options: {
                  sourceMap: false,
                  importLoaders: 2,
                  modules: {
                    localIdentName: `[name]_[local]_[hash:base64:5]`
                  }
                }
              },
              /* config.module.rule(`stylus`).oneOf(`vue-modules`).use(`postcss-loader`) */
              {
                loader: `${PASTH.mainPath}node_modules\\postcss-loader\\src\\main.js`,
                options: {
                  sourceMap: false,
                  plugins: [
                    function () { /* omitted long function */ }
                  ]
                }
              },
              /* config.module.rule(`stylus`).oneOf(`vue-modules`).use(`stylus-loader`) */
              {
                loader: `stylus-loader`,
                options: {
                  sourceMap: false,
                  preferPathResolver: `webpack`
                }
              }
            ]
          },
          /* config.module.rule(`stylus`).oneOf(`vue`) */
          {
            resourceQuery: /\?vue/,
            use: [
              /* config.module.rule(`stylus`).oneOf(`vue`).use(`vue-style-loader`) */
              {
                loader: `${PASTH.mainPath}node_modules\\vue-style-loader\\main.js`,
                options: {
                  sourceMap: false,
                  shadowMode: false
                }
              },
              /* config.module.rule(`stylus`).oneOf(`vue`).use(`css-loader`) */
              {
                loader: `${PASTH.mainPath}node_modules\\css-loader\\dist\\cjs.js`,
                options: {
                  sourceMap: false,
                  importLoaders: 2
                }
              },
              /* config.module.rule(`stylus`).oneOf(`vue`).use(`postcss-loader`) */
              {
                loader: `${PASTH.mainPath}node_modules\\postcss-loader\\src\\main.js`,
                options: {
                  sourceMap: false,
                  plugins: [
                    function () { /* omitted long function */ }
                  ]
                }
              },
              /* config.module.rule(`stylus`).oneOf(`vue`).use(`stylus-loader`) */
              {
                loader: `stylus-loader`,
                options: {
                  sourceMap: false,
                  preferPathResolver: `webpack`
                }
              }
            ]
          },
          /* config.module.rule(`stylus`).oneOf(`normal-modules`) */
          {
            test: /\.module\.\w+$/,
            use: [
              /* config.module.rule(`stylus`).oneOf(`normal-modules`).use(`vue-style-loader`) */
              {
                loader: `${PASTH.mainPath}node_modules\\vue-style-loader\\main.js`,
                options: {
                  sourceMap: false,
                  shadowMode: false
                }
              },
              /* config.module.rule(`stylus`).oneOf(`normal-modules`).use(`css-loader`) */
              {
                loader: `${PASTH.mainPath}node_modules\\css-loader\\dist\\cjs.js`,
                options: {
                  sourceMap: false,
                  importLoaders: 2,
                  modules: {
                    localIdentName: `[name]_[local]_[hash:base64:5]`
                  }
                }
              },
              /* config.module.rule(`stylus`).oneOf(`normal-modules`).use(`postcss-loader`) */
              {
                loader: `${PASTH.mainPath}node_modules\\postcss-loader\\src\\main.js`,
                options: {
                  sourceMap: false,
                  plugins: [
                    function () { /* omitted long function */ }
                  ]
                }
              },
              /* config.module.rule(`stylus`).oneOf(`normal-modules`).use(`stylus-loader`) */
              {
                loader: `stylus-loader`,
                options: {
                  sourceMap: false,
                  preferPathResolver: `webpack`
                }
              }
            ]
          },
          /* config.module.rule(`stylus`).oneOf(`normal`) */
          {
            use: [
              /* config.module.rule(`stylus`).oneOf(`normal`).use(`vue-style-loader`) */
              {
                loader: `${PASTH.mainPath}node_modules\\vue-style-loader\\main.js`,
                options: {
                  sourceMap: false,
                  shadowMode: false
                }
              },
              /* config.module.rule(`stylus`).oneOf(`normal`).use(`css-loader`) */
              {
                loader: `${PASTH.mainPath}node_modules\\css-loader\\dist\\cjs.js`,
                options: {
                  sourceMap: false,
                  importLoaders: 2
                }
              },
              /* config.module.rule(`stylus`).oneOf(`normal`).use(`postcss-loader`) */
              {
                loader: `${PASTH.mainPath}node_modules\\postcss-loader\\src\\main.js`,
                options: {
                  sourceMap: false,
                  plugins: [
                    function () { /* omitted long function */ }
                  ]
                }
              },
              /* config.module.rule(`stylus`).oneOf(`normal`).use(`stylus-loader`) */
              {
                loader: `stylus-loader`,
                options: {
                  sourceMap: false,
                  preferPathResolver: `webpack`
                }
              }
            ]
          }
        ]
      },
      /* config.module.rule(`js`) */
      {
        test: /\.m?jsx?$/,
        exclude: [
          function () { /* omitted long function */ }
        ],
        use: [
          /* config.module.rule(`js`).use(`cache-loader`) */
          {
            loader: `${PASTH.mainPath}node_modules\\cache-loader\\dist\\cjs.js`,
            options: {
              cacheDirectory: `${PASTH.mainPath}node_modules\\.cache\\babel-loader`,
              cacheIdentifier: `6271c33e`
            }
          },
          /* config.module.rule(`js`).use(`babel-loader`) */
          {
            loader: `${PASTH.mainPath}node_modules\\babel-loader\\lib\\main.js`
          }
        ]
      },
      /* config.module.rule(`eslint`) */
      {
        enforce: `pre`,
        test: /\.(vue|(j|t)sx?)$/,
        exclude: [
          /node_modules/,
          `${PASTH.mainPath}node_modules\\@vue\\cli-service\\lib`
        ],
        use: [
      //     /* config.module.rule(`eslint`).use(`eslint-loader`) */
          {
            loader: `${PASTH.mainPath}node_modules\\eslint-loader\\main.js`,
            options: {
              extensions: [
                `.js`,
                `.jsx`,
                `.vue`
              ],
              cache: false,
              cacheIdentifier: `f0ed5ea6`,
              emitWarning: false,
              emitError: false,
              eslintPath: `${PASTH.mainPath}node_modules\\eslint`,
              formatter: function () { /* omitted long function */ }
            }
          }
        ]
      }
    ]
  },
  optimization: {
    splitChunks: {
      cacheGroups: {
        vendors: {
          name: `chunk-vendors`,
          test: /[\\\/]node_modules[\\\/]/,
          priority: -10,
          chunks: `initial`
        },
        common: {
          name: `chunk-common`,
          minChunks: 2,
          priority: -20,
          chunks: `initial`,
          reuseExistingChunk: true
        }
      }
    },
    minimizer: [
      /* config.optimization.minimizer(`terser`) */
      new TerserPlugin(
        {
          terserOptions: {
            compress: {
              arrows: false,
              collapse_vars: false,
              comparisons: false,
              computed_props: false,
              hoist_funs: false,
              hoist_props: false,
              hoist_vars: false,
              inline: false,
              loops: false,
              negate_iife: false,
              properties: false,
              reduce_funcs: false,
              reduce_vars: false,
              switches: false,
              toplevel: false,
              typeofs: false,
              booleans: true,
              if_return: true,
              sequences: true,
              unused: true,
              conditionals: true,
              dead_code: true,
              evaluate: true
            },
            mangle: {
              safari10: true
            }
          },
          sourceMap: true,
          cache: true,
          parallel: true,
          extractComments: false
        }
      )
    ]
  },
  plugins: [
    /* config.plugin(`vue-loader`) */
    new VueLoaderPlugin(),
    /* config.plugin(`define`) */
    new DefinePlugin(
      {
        'process.env': {
          NODE_ENV: `"development"`,
          BASE_URL: `"/"`
        }
      }
    ),
    /* config.plugin(`case-sensitive-paths`) */
    new CaseSensitivePathsPlugin(),
    /* config.plugin(`friendly-errors`) */
    new FriendlyErrorsWebpackPlugin(
      {
        additionalTransformers: [
          function () { /* omitted long function */ }
        ],
        additionalFormatters: [
          function () { /* omitted long function */ }
        ]
      }
    ),
    /* config.plugin(`html`) */
    new HtmlWebpackPlugin(
      {
        templateParameters: function () { /* omitted long function */ },
        template: `${PASTH.mainPath}src\\index.html`
      }
    ),
    /* config.plugin(`preload`) */
    new PreloadPlugin(
      {
        rel: `preload`,
        include: `initial`,
        fileBlacklist: [
          /\.map$/,
          /hot-update\.js$/
        ]
      }
    ),
    /* config.plugin(`prefetch`) */
    new PreloadPlugin(
      {
        rel: `prefetch`,
        include: `asyncChunks`
      }
    ),
    /* config.plugin(`copy`) */
    new CopyPlugin(
      [
        {
          from: `${PASTH.mainPath}src`,
          to: `${PASTH.mainPath}dist`,
          toType: `dir`,
          ignore: [
            '.DS_Store',
            {
              glob: `index.html`,
              matchBase: false
            }
          ]
        }
      ]
    )
  ],
  entry: {
    app: [
      `./src/main.js`
    ]
  },
}
