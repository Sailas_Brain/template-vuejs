
import Vue from 'vue'
import VueRouter from 'vue-router'
import Goods from '../components/page/Goods.vue'
import Test from '../components/page/Test.vue'
import Home from '../components/Home.vue'
import CubeEffect from '../components/page/CubeEffect.vue'
import ColorPalette from '../components/page/ColorPalette.vue'
import Spiro from '../components/page/Spiro.vue'
import Particles from '../components/page/Particles.vue'
import Lightnings from '../components/page/Lightnings.vue'
Vue.use(VueRouter);
export default new VueRouter 
({
  routes:
  [
    {
        path: '/',
        name: 'Home',
        component: Home
    }, 
    {
        path: '/test',
        name: 'test',
        component: Test
    }, 
    {
        path: '/Goods', 
        name: 'Goods',
        component: Goods
    }, 
    {
        path: '/CubeEffect', 
        name: 'CubeEffect',
        component: CubeEffect
    }, 
    {
        path: '/ColorPalette', 
        name: 'ColorPalette',
        component: ColorPalette
    }, 
    {
        path: '/Spiro', 
        name: 'Spiro',
        component: Spiro
    }, 
    {
        path: '/Particles', 
        name: 'Particles',
        component: Particles
    }, 
    {
        path: '/Lightnings',
        name: 'Lightnings',
        component: Lightnings
    }
  ]
})

