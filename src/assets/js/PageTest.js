export default {
  data() {
    return {
    }
  },
  props: {
    msg: String
  },
  methods: {
    circleMove: () =>
    {
        const canvasCircle = document.getElementById("circle");
        const context = canvasCircle.getContext('2d');

        const w = canvasCircle.width;
        const h = canvasCircle.height;
        const arrayCircle = [];
        const circleRadius1 = 250;
        const circleRadius2 = 50;
        const TWO_PI = 2*Math.PI;

        class Circle {
          constructor()
          {
            this.x = w/2;
            this.y = h/2;
          }
          draw()
          {
            context.lineWidth = 3;
            context.strokeStyle = `black`;
            context.beginPath();
            context.arc(this.x, this.y, circleRadius1, 0, TWO_PI);
            context.closePath();
            context.stroke();
          }
          onDrawMove(x, y)
          {
            // context.beginPath();
            // context.clearRect(this.x - circleRadius1 - 5, this.y - circleRadius1 - 5, circleRadius1 * 2 + 2, circleRadius1 * 2 + 2);
            // context.closePath();

            this.x = (x + circleRadius1) || this.x;
            this.y = (y + circleRadius1) || this.y;


            context.clearRect(0, 0, w, h);
            context.lineWidth = 1;
            context.fillStyle = `grey`;
            context.beginPath();
            context.arc(this.x, this.y, circleRadius2, 0, TWO_PI);
            context.closePath();
            context.stroke();
            context.fill();
          }
        }
        canvasCircle.onmousemove = function()
        {
          let mx = w/2 - event.offsetX;
          let my = h/2 - event.offsetY;
          // alert(mx+"  "+my);
          for (let i = 0; i < arrayCircle.length; i++) {
              arrayCircle[i].onDrawMove(-mx, -my);
          }
        }
        const loop = function()
        {
          for (let i = 0; i < arrayCircle.length; i++) {
            arrayCircle[i].draw();
          }
        }
        const init = function()
        {
          arrayCircle.push(new Circle);
        }
        init();
        loop();











        // canvasCircle.onmousemove = () =>
        // {
        //     context.beginPath();
        //     context.clearRect(0, 0, 500, 500);
        //     const x = event.offsetX;
        //     const y = event.offsetY;
        //     context.arc(x, y, 50, 0, pi, false);
        //     context.lineWidth = '3';
        //     context.fillStyle = "grey";
        //     context.stroke();
        //     context.fill();
        // }




    },
  },
  mounted() {
    this.circleMove();
  },
}





// ctx.drawImage(Image, dX, dY, dWidth, dHeight); // добавление картинок  



