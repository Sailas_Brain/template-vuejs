export default
{
    data() {
        return {
            
        }
    },
    mounted() {
        const canvasSpiro = document.getElementById("spiro");
        const ctx = canvasSpiro.getContext('2d');
        let R = 107; // 15 107
        let r = 151; // 174 151
        let d = 150; // 99 150
        let teta = 0;
        // let timeout;
        let spiroColor = "black";
        document.getElementById("radius-1").oninput = function()
        {
            ctx.clearRect(0,0, 600, 600);
            R = event.target.value;
            ctx.beginPath();
        };
        document.getElementById("radius-2").oninput = function()
        {
            ctx.clearRect(0,0, 600, 600);
            r= event.target.value;
            ctx.beginPath();
        };
        document.getElementById("diameter").oninput = function()
        {
            ctx.clearRect(0,0, 600, 600);
            d = event.target.value;
            ctx.beginPath();
        };
        document.getElementById("spiroColor").oninput = function()
        {
            ctx.clearRect(0,0, 600, 600);
            spiroColor = event.target.value;
            ctx.beginPath();
        };
        const spiro = function()
        {
            const x = (R - r)*Math.cos(teta) + d*Math.cos( (R - r)*teta/r );
            const y = (R - r)*Math.sin(teta) - d*Math.sin( (R - r)*teta/r );
            teta = teta+0.1;
            ctx.fillStyle = spiroColor;
            ctx.strokeStyle = spiroColor;
            ctx.lineWidth = 3 * 2;
            ctx.lineTo(300+x*1.5,300+y*1.5);
            ctx.stroke();
            
            ctx.beginPath();
            ctx.arc(300+x*1.5, 300+y*1.5, 3, 0, Math.PI*2);
            ctx.closePath();
            ctx.fill();

            ctx.beginPath();
            ctx.moveTo(300+x*1.5,300+y*1.5);
            

            // timeout = setTimeout(spiro, 50);
            requestAnimationFrame(spiro);
        };
        spiro();
    },
}
