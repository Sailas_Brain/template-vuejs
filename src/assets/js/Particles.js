export default
{
    data() {
        return {
            
        }
    },
    mounted() {
        let canvasParticle = document.getElementById("Particles__particlesCanvas");
        let ctx = canvasParticle.getContext("2d");
        let w = canvasParticle.width = innerWidth;
        let h = canvasParticle.height = innerHeight;
        let particles = [];
        let properties = 
        {
            bgColor: "rgba(17, 14, 255, 1)",
            particleColor: "rgba(255,255,255,1)",
            particleRadius: 2,
            particleCount: 200,
            particleMaxVelocity: 0.5,
            lineLength: 170,
            particleLife: 6,
        }
        window.onresize = function()
        {
            w = canvasParticle.width = innerWidth;
            h = canvasParticle.height = innerHeight;
        }
        class Particle {

            constructor()
            {
                this.x = Math.random()*w;
                this.y = Math.random()*h;
                this.velocityX = Math.random()*(properties.particleMaxVelocity*2) - properties.particleMaxVelocity;
                this.velocityY = Math.random()*(properties.particleMaxVelocity*2) - properties.particleMaxVelocity;
                this.life = Math.random()*properties.particleLife*60;
            }
            position()
            {
                this.x + this.velocityX > w && this.velocityX > 0 || this.x + this.velocityX < 0 && this.velocityX < 0 ? this.velocityX*=-1 : this.velocityX;
                this.y + this.velocityY > h && this.velocityY > 0 || this.y + this.velocityY < 0 && this.velocityY < 0 ? this.velocityY*=-1 : this.velocityY;
                this.x += this.velocityX;
                this.y += this.velocityY;
            }
            reDraw()
            {
                ctx.beginPath();
                ctx.arc(this.x, this.y, properties.particleRadius, 0, Math.PI*2);   
                ctx.closePath();
                ctx.fillStyle = properties.particleColor;
                ctx.fill();
            }
            reCalculateLife()
            {
                if(this.life < 1)
                {
                    this.x = Math.random()*w;
                    this.y = Math.random()*h;
                    this.velocityX = Math.random()*(properties.particleMaxVelocity*2) - properties.particleMaxVelocity;
                    this.velocityY = Math.random()*(properties.particleMaxVelocity*2) - properties.particleMaxVelocity;
                    this.life = Math.random()*properties.particleLife*60;
                }
                this.life--;
            }
        }
        const reDrawBackground = function()
        {
            ctx.fillStyle = properties.bgColor;
            ctx.fillRect(0, 0, w, h);
        }
        const drawLines = function()
        {
            let x1, y1, x2, y2, length, opacity;
            for(let i in particles)
            {
                for(let j in particles)
                {
                    x1 = particles[i].x;
                    y1 = particles[i].y;
                    x2 = particles[j].x;
                    y2 = particles[j].y;
                    length = Math.sqrt(Math.pow(x2 - x1, 2) + Math.pow(y2 - y1, 2)); // формула диагонали Math.sqrt(a2 - b2, 2);
                    if(length < properties.lineLength)
                    {
                        opacity = 1 - length/properties.lineLength;
                        ctx.lineWidth = "0.5";
                        ctx.strokeStyle = "rgba(0,130,233, "+ opacity +"";
                        ctx.beginPath();
                        ctx.moveTo(x1, y1);
                        ctx.lineTo(x2, y2);
                        ctx.closePath();
                        ctx.stroke();
                    }
                }
            }
        }
        const reDrawParticles = function()
        {
            for (let i in particles) {
                particles[i].reCalculateLife();
                particles[i].position();
                particles[i].reDraw();
            }
        }
        const loop = function()
        {
            reDrawBackground();
            reDrawParticles();
            drawLines();
            requestAnimationFrame(loop);
        }
        const init = function()
        {
            for (let i = 0; i < properties.particleCount; i++) {
                particles.push( new Particle);
            }
            loop();
        }
        init();
    },
}
