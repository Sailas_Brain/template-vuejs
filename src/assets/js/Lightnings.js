export default
{
    data() {
        return {
            
        }
    },
    mounted() {
        const canvasLightnings = document.getElementById("Lightnings__canvas-lightnings");
        const ctx = canvasLightnings.getContext("2d");
        let w = canvasLightnings.width = 1200;
        let h = canvasLightnings.height = innerHeight;
        let canvasColor = `#232332`;
        const circles = [];
        const circlesCount = 4;
        let mx = 0;
        let my = 0;
        let toggle = 0;
        const TWO_PI = Math.PI*2;
        const stepLength = 2;
        const maxOffset = 6; 
        const maxLength = 500;

        class Circle {
            constructor(x, y)
            {
                this.x = x || Math.random() *w;
                this.y = y || Math.random() *h;
            }
            draw(x, y)
            {
                this.x = x || this.x;
                this.y = y || this.y;

                ctx.lineWidth = 1.5;
                ctx.fillStyle = `white`;
                ctx.strokeStyle = `red`;

                ctx.beginPath();
                ctx.arc(this.x, this.y, 6, 0, TWO_PI);
                ctx.closePath();
                ctx.fill();

                ctx.beginPath();
                ctx.arc(this.x, this.y, 28, 0, TWO_PI);
                ctx.closePath();
                ctx.stroke();
            }
        }

        const createLinghtning = function()
        {
            for (let i = 0; i < circles.length; i++) {
                for (let j = i+1; j < circles.length; j++) {
                    let dist = getDistance(circles[i], circles[j]);
                    let chance = dist / maxLength;
                    if(chance > Math.random()) continue;
                    let otherColor = chance * 255;
                    let stepsCount = dist / stepLength;
                    let sx = circles[i].x;
                    let sy = circles[i].y;
                    ctx.lineWidth = 2.5;
                    ctx.strokeStyle = `rgb(255, ${otherColor}, ${otherColor})`;
                    ctx.beginPath();
                    ctx.moveTo(circles[i].x, circles[i].y);
                    for (let t = stepsCount; t > 1; t--) {
                        let pathLength = getDistance(circles[i], {x: sx, y: sy});
                        let offset = Math.sin(pathLength / dist * Math.PI) * maxOffset;

                        sx += (circles[j].x - sx) / t + Math.random() * offset * 2 - offset;
                        sy += (circles[j].y - sy) / t + Math.random() * offset * 2 - offset;
                        ctx.lineTo(sx, sy);
                    }
                    ctx.stroke();
                }
            }
        }

        const getDistance = function(a, b)
        {
            return Math.sqrt(Math.pow(b.x - a.x, 2) + Math.pow(b.y - a.y, 2)); //Теорема Пифагора 
        }

        canvasLightnings.onmousemove = function(evt)
        {
            mx = evt.x - canvasLightnings.getBoundingClientRect().x;
            my = evt.y - canvasLightnings.getBoundingClientRect().y;
        }
        window.onkeydown = function()
        {
            toggle == circles.length - 1 ? toggle = 0 : toggle++;
        }


        const loop = function()
        {
            ctx.clearRect(0, 0, w, h);
            createLinghtning();
            circles.map(i => { i == circles[toggle] ? i.draw(mx, my) : i.draw() })

            requestAnimationFrame(loop);
        }

        const init = function()
        {
            for (let i = 0; i < circlesCount; i++)
            {
                circles.push( new Circle );
            }
            canvasLightnings.style.background = canvasColor;
        }
        init();
        loop();
    },
}




