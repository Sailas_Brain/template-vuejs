export default
{
    data() {
        return {
            
        }
    },
    mounted() {
        let myColor;
        let mySize = 5;
        const InputColor = document.getElementById("addColor");
        const InputSize = document.getElementById("addSize");
        const convasColor = document.getElementById("color");
        const clearFon = document.getElementById("clearFon");
        const ctx = convasColor.getContext('2d');
        InputColor.addEventListener("input", function(evt)
        {
            myColor = evt.target.value;
        });
        InputSize.addEventListener("input", function(evt)
        {
            mySize = evt.target.value;
        });
        convasColor.addEventListener("mousedown", function()
        {
            convasColor.onmousemove = () =>
            {
                const x = event.offsetX;
                const y = event.offsetY;
            
                ctx.lineWidth = mySize * 2;
                ctx.strokeStyle = myColor;
                ctx.lineTo(x,y);
                ctx.stroke();
                ctx.closePath();
                

                ctx.beginPath();
                ctx.fillStyle = myColor;
                ctx.arc(x, y, mySize, 0, Math.PI*2);
                ctx.fill();
                

                ctx.beginPath();
                ctx.moveTo(x,y);
                
            }  
            convasColor.onmouseup = () =>
            {
                convasColor.onmousemove = null;
                ctx.beginPath();
            }
        });
        clearFon.addEventListener("click", function()
        {
            ctx.clearRect(0, 0, 500, 600);
            convasColor.onmousemove = null;
        })
    },
}